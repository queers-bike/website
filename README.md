# Queers bike website

Website of the queersbike.eu.org community.

Made from static files only.
```
├── .gitignore
├── LICENSE
├── README.md
├── acknowledgements.html
├── assets
│   ├── css
│   │   └── styles.css
│   ├── favicons
│   │   ├── android-chrome-192x192.png
│   │   ├── android-chrome-512x512.png
│   │   ├── apple-touch-icon.png
│   │   ├── browserconfig.xml
│   │   ├── favicon-16x16.png
│   │   ├── favicon-32x32.png
│   │   ├── favicon.ico
│   │   ├── mstile-150x150.png
│   │   ├── safari-pinned-tab.svg
│   │   └── site.webmanifest
│   └── images
│       ├── banner.avif
│       ├── banner.jpg
│       └── banner.webp
├── index.html
├── robots.txt
└── sitemap.xml
```

Compliance:
- Accessibility
	- [WCAG 2.1](https://www.w3.org/TR/WCAG21/), [AAA level](https://wave.webaim.org/report#/https://queersbike.eu.org)
	- [RGAA](https://accessibilite.numerique.gouv.fr)
- Markup
	- [HTML5](https://validator.w3.org/nu/?doc=https%3A%2F%2Fqueersbike.eu.org)
	- [CSS3](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fqueersbike.eu.org&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=en)
